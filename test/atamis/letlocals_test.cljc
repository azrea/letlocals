(ns atamis.letlocals-test
  (:require [atamis.letlocals :refer [letlocals]]
            #?(:clj [clojure.test :as t]
               :cljs [cljs.test :as t :include-macros true])))

(t/deftest test-letlocals
  (t/is (= nil (letlocals)))
  (t/is (= 1 (letlocals 1)))
  (let [v (volatile! nil)]
    ;; This tests too much in one go
    (t/is (= 12 (letlocals
                (bind x 1
                      y 3)
                (vreset! v (* y 2))
                (bind n1 (+ x y)
                      n2 (* n1 3)))))
    (t/is (= @v 6 ))))

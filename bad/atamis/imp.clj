(ns atamis.imp)

;; This is another version of letlocals whose code isn't net less-complex.

(def thus nil)
(def ... nil)
(def local nil)

(defn ^:private is-thus?
  [expr]
  (and (symbol? expr) (#{#'thus #'...} (resolve expr))))

(defn ^:private local-form-bindings
  "Takes a local form and returns the full bindings as formatted for let.

  (local x 1 y 2) => [x 1 y 2]

  (local [x 1 y 2]) => [x 1 y 2]
  "
  [form]
  (let [args (rest form)]
    (if (vector? (first args))
      (first args)
      (vec args))))

(defmacro imp
  "Macro for pseudo-imperative code . Works like `do`, executing all the forms
  and returning the value of the last form, but with some special forms. These
  forms should be referred using the vars in this namespace, although they
  cannot be used outside the `imp` macro. These special forms can modify the
  return value.

    (require [atamis.imp :refer [imp ... local]])


    (let [counter (atom 0)]
      (imp/imp
        (loop [l (range 100)] imp/...)
        (when (seq l) imp/...)
        (swap! counter inc)
        (recur (rest l)))
      @counter)
    => 100"
  [& args]
  (if (empty? args)
    `nil
    (let [[f & r] args]
      (cond
        ;; Local
        (and (seq? f) (= #'local (resolve (first f))))
        `(let ~(local-form-bindings f)
           ~(when (seq r)
              `(imp ~@r)))

        ;; Replace thus
        (and (seq? f) (some is-thus? f))
        (map
         (fn [expr]
           (if (is-thus? expr)
             `(imp ~@r)
             expr
             ))
         f)

        ;; No further exprs means we just return the current one. Note that this
        ;; only handles normal exprs. Special forms above need to deal with no
        ;; remaining exprs in their own way.
        (empty? r)
        f

        :else
        `(do
           ~f
           (imp ~@r))))))

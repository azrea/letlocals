(ns atamis.letlocals)

(defmacro letlocals
  "Macro to evaluate expressions, using `(bind name value...)` to bind local
  values for the rest of the expression.

      (letlocals
       (bind x 1
             y 3)
       (assert x y)
       (+ x y))
  "
  [& args]
  (let [is-bind? #(and (list? %) (= (first %) 'bind) )
        ;; This is alerternating names and expressions
        elements (mapcat #(if (is-bind? %) (rest %) ['_ %] ) args)
        num-el (count elements)
        ;; -1 for count to index and -1 for second to last
        idx-second-last? #(= % (- num-el 2) )
        ;; The symbol used to bind the answer
        ans-sym (gensym)]
    `(let ~(->> elements
                (map-indexed
                 ;; replace second to last bind with answer symbol
                 (fn [i x]
                   (if (idx-second-last? i)
                     ans-sym
                     x)))
                (into []))
       ;; Zero elements has to be special cased as ans-sym won't be bound
       ~(if (= num-el 0)
          nil
          ans-sym))))
